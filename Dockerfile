FROM alpine:3.2

MAINTAINER Aldrin Leal <aldrin@leal.eng.br>

RUN apk --update add python

RUN mkdir /htdocs

ADD htdocs /htdocs

CMD cd /htdocs ; python -mSimpleHTTPServer

EXPOSE 8000
